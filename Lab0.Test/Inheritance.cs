﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using h = PK.Test.Helpers;

using Lab0.Main;

namespace Lab0.Test
{
    [TestFixture]
    [TestClass]
    public class Inheritance
    {
        [Test]
        [TestMethod]
        public void B_Should_Inherit_From_A()
        {
            h.Should_Inherit_From(LabDescriptor.B, LabDescriptor.A);
        }

        [Test]
        [TestMethod]
        public void C_Should_Inherit_From_A()
        {
            h.Should_Inherit_From(LabDescriptor.C, LabDescriptor.A);
        }
    }
}
