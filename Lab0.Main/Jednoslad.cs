﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Jednoslad:Pojazd
    {
        private int moc;
        public Jednoslad()
        { moc = 240; }

        public override string Jedz()
        {
            return "jade motorem o mocy KM:" + Convert.ToString(moc); 
        }
    }
}
