﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Dwuslad:Pojazd
    {
        private int moc;
        public Dwuslad()
        { moc = 150; }

        public override string Jedz()
        {
            return "jade samochodem o mocy KM:" + Convert.ToString(moc);
        }
    }
}
