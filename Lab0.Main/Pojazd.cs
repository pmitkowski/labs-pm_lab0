﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class Pojazd
    {
        private int moc;
        public Pojazd()
        {
            moc = 100;
        }
        public virtual string Jedz()
        {
            return "jade pojazdem o mocy KM:" + Convert.ToString(moc);
        }
    }
}
